package sinkhole

import (
	"bufio"
	"context"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
	"log"
	"os"
	"regexp"
)

func Run(ctx context.Context, cfg *Config) error {
	err := cfg.initSources()
	if err != nil {
		return err
	}
	if cfg.dbd() != "" {
		return dbd(ctx, cfg)
	}
	return traditional(ctx, cfg)
}

func traditional(ctx context.Context, cfg *Config) error {
	g, gcx := errgroup.WithContext(ctx)

	fq := pool(gcx, g, cfg)
	de := dedup(gcx, g, fq)
	al := allow(gcx, g, cfg, de)

	// duplicate the names/channel
	ch1, ch2 := tee(gcx.Done(), al)
	out1, out2 := tee(gcx.Done(), ch2)

	pt := passthru(gcx, g, cfg, ch1)
	dnscryptFormat(gcx, g, pt)

	unboundFormat(gcx, g, out1)
	dnsmasqFormat(gcx, g, out2)

	// group is accumulating the goroutine errors
	if err := g.Wait(); err != nil {
		return err
	}
	return nil
}

// pool creates the channel of domain names from the many sources
func pool(ctx context.Context, g *errgroup.Group, cfg *Config) chan interface{} {
	// names channel will be our domain names to be processed downstream
	var ch = make(chan interface{})

	g.Go(func() error {
		defer close(ch)
		var (
			src     source
			name    string
			ok      bool
			scanner *bufio.Scanner
			skip    = cfg.skipping()
		)

		// todo can we range over sources list?
		for {
			if src, ok = cfg.nextSource(); !ok {
				// reached end-of-sources
				break
			}

			xfer := newTransport(cfg)
			r, err := xfer.fetch(ctx, src)
			if err != nil {
				if !skip {
					return errors.Wrap(err, "Stopped fetch")
				}
				log.Printf("Skipping error: %v", err)
				continue
			}

			scanner = bufio.NewScanner(r)
			for scanner.Scan() {
				name = src.fqdn(scanner.Text())
				if name == "" {
					continue
				}

				select {
				case ch <- name:
				case <-ctx.Done():
					return ctx.Err()
				}
			}
			if err := scanner.Err(); err != nil {
				return err
			}
		}
		return nil
	})
	return ch
}

func dedup(ctx context.Context, g *errgroup.Group, names <-chan interface{}) chan interface{} {
	// For our fold transform process, take the raw domain names
	// from the sources, and create a reduced set of domain names
	// by culling duplicates, folding subdomains under their zones

	var (
		ch = make(chan interface{})
		fq string
		zs = newZones()
	)

	// sequentially build set
	for name := range names {
		fq = name.(string)
		if zs.lookup(fq) {
			continue
		}
		zs.insert(fq)
	}
	// sequentially reduce set
	zs.deduplicate()
	// now set is prepared

	g.Go(func() error {
		defer close(ch)
		for _, v := range zs.members() {
			select {
			case ch <- v:
			case <-ctx.Done():
				return ctx.Err()
			}
		}

		return nil
	})

	return ch
}

func allow(ctx context.Context, g *errgroup.Group, cfg *Config, names <-chan interface{}) chan interface{} {
	// For our filter transform process, take the blocked domain names
	// and create a reduced set of domain names
	// by excluding domains that are allowed
	var ch = make(chan interface{})

	g.Go(func() error {
		defer close(ch)
		al, err := allowSubset(cfg.allow())
		if err != nil {
			return err
		}

		for name := range names {
			if ok := al.lookup(name.(string)); ok {
				// Domain is a member of the allow set, skip.
				continue
			}

			select {
			case ch <- name:
			case <-ctx.Done():
				return ctx.Err()
			}
		}

		return nil
	})
	return ch
}

func passthru(ctx context.Context, g *errgroup.Group, cfg *Config, names <-chan interface{}) chan interface{} {
	// pass-through of the local additions (rules)
	// which is custom to dnscrypt-proxy filtering

	var (
		path = cfg.localAdds()
		re   = regexp.MustCompile(`^[a-z0-9-\.\*]+`)
		ch   = make(chan interface{})
	)
	g.Go(func() error {
		defer close(ch)

		r, err := os.Open(path)
		if err != nil {
			return err
		}

		scanner := bufio.NewScanner(r)
		for scanner.Scan() {
			rule := re.FindString(scanner.Text())
			if rule == "" {
				continue
			}

			select {
			case ch <- rule:
			case <-ctx.Done():
				return ctx.Err()
			}
		}
		if err := scanner.Err(); err != nil {
			return err
		}

		// append names channel into ch
		for name := range names {
			ch <- name
		}

		return nil
	})
	return ch
}

// set of domain names that are allowed
func allowSubset(path string) (zones, error) {
	var (
		rule    string
		scanner *bufio.Scanner
		zo      = newZones()
	)
	r, err := os.Open(path)
	if err != nil {
		return zo, err
	}

	scanner = bufio.NewScanner(r)
	for scanner.Scan() {
		rule = reGeneric.FindString(scanner.Text())
		if rule == "" {
			continue
		}
		if zo.lookup(rule) {
			continue
		}
		zo.insert(rule)
	}
	if err := scanner.Err(); err != nil {
		return zo, err
	}
	return zo, nil
}
