package sinkhole

import (
	"math/rand"
	"testing"
	"time"
)

func TestZoneInsert(t *testing.T) {
	var f = newZones()

	// zones (level-1) which cover the regular expressions
	var zo = []string{"adafruit.com", "gov.bc.ca", "toronto.ca"}

	// Test call to insert
	for _, v := range zo {
		f.insert(v)
	}

	// Verify the insert call results
	for k, _ := range f.internal {
		switch k {
		case zo[0]:
		case zo[1]:
		case zo[2]:
			continue

		default:
			// Unexpected key; they should match our zones slice
			t.Errorf("Expected a value from %v, got %v", zo, k)
			break
		}
	}
}

func TestSubdomainInsert(t *testing.T) {
	var (
		z = newZones()
		// subdomains with descendants for least/greatest and split
		sd = []string{"www1.example.com", "apple.example.com", "xanadu.example.com"}
		lt = []string{"www1.example.com", "apple.example.com", "adam.example.com"}
		rt = []string{"www1.example.com", "xanadu.example.com", "yellow.example.com"}
	)
	// Test call to insert for split descendants
	z.insert(sd[0])

	if !z.lookup(sd[0]) {
		t.Errorf("Expected insert call to work")
	}

	// Test the call to insert
	z.insert(sd[1])
	z.insert(sd[2])

	// Verify the left child subdomain
	if !z.lookup(sd[1]) {
		t.Errorf("Expected %v, got %v", sd[1], z.lookup(sd[1]))
	}

	// Verify the right child subdomain
	if !z.lookup(sd[2]) {
		t.Errorf("Expected %v, got %v", sd[2], z.lookup(sd[2]))
	}

	// Test for least most descendant in structure
	z = newZones()
	z.insert(lt[0])
	z.insert(lt[1])
	z.insert(lt[2])

	if !z.lookup(lt[2]) {
		t.Errorf("Expected %v, got %v", lt[2], z.lookup(lt[2]))
	}

	// Test for greatest descendant in structure
	z = newZones()
	z.insert(rt[0])
	z.insert(rt[1])
	z.insert(rt[2])

	if !z.lookup(rt[2]) {
		t.Errorf("Expected %v, got %v", rt[2], z.lookup(rt[2]))
	}
}

func TestDescendants(t *testing.T) {
	var (
		zs = newZones()
		// Subdomains for example.com zone
		fq = []string{"www1.example.com", "apple.example.com", "xanadu.example.com"}
	)
	// Test call to insert first subdomain
	zs.insert(fq[0])
	// Dump set members out to slice
	var m = zs.members()
	// Verify result is size one and subdomain is the same
	if len(m) != 1 {
		t.Errorf("Expected 1 member, got %d", len(m))
	}
	if m[0] != fq[0] {
		t.Errorf("Expected name %s, got %s", fq[0], m[0])
	}

	// Test call to insert remaining subdomains
	zs.insert(fq[1])
	zs.insert(fq[2])
	// Dump set members out to slice
	m = zs.members()
	if len(m) != 3 {
		t.Errorf("Expected 3 member, got %d", len(m))
	}
	for _, v := range m {
		switch v {
		case fq[0]:
		case fq[1]:
		case fq[2]:
			continue
		default:
			t.Errorf("Expected name from %v, got %s", fq, v)
		}
	}

	// Test set members list with different zones
	zs = newZones()
	var fq2 = []string{"www1.example.com", "apple.apple.com", "xanadu.xanadu.com"}
	zs.insert(fq2[0])
	zs.insert(fq2[1])
	zs.insert(fq2[2])
	// Dump set members list out to slice
	m = zs.members()
	// Verify result size and slice data
	if len(m) != 3 {
		t.Errorf("Expected 3 member, got %d", len(m))
	}
	for _, v := range m {
		switch v {
		case fq2[0]:
		case fq2[1]:
		case fq2[2]:
			// members match data from input subdomains
			continue
		default:
			// A member is unexpected and doesn't match
			t.Errorf("Expected item from %v, got %s", fq2, v)
		}
	}

	// zone names / level-1 domains should produce zero descendants
	zs = newZones()
	var fq3 = []string{"example.com", "apple.com", "xanadu.com"}
	zs.insert(fq3[0])
	zs.insert(fq3[1])
	zs.insert(fq3[2])
	// Dump set members list out to slice
	m = zs.members()
	// Verify size
	if len(m) != len(fq3) {
		t.Errorf("Expected 3 member, got %d", len(m))
	}
}

func BenchmarkSubdomainLookup(b *testing.B) {
	rand.Seed(time.Now().UnixNano())
	var (
		zs     = sampleSubdomains()
		j      int
		max    = len(fq)
		target string
	)
	for i := 0; i < b.N; i++ {
		j = rand.Intn(max)
		target = fq[j]
		zs.lookup(target)
	}
}

var (
	// Subdomains for 2 zones
	fq = []string{
		"www1.example.com", "apple.example.com", "xanadu.example.com", "target.example.com",
		"www2.tempuri.com", "apple.tempuri.com", "xanadu.tempuri.com", "target.tempuri.com",
		"echo.example.com", "foxtrot.example.com", "golf.example.com", "pineapple.example.com",
	}
)

func sampleSubdomains() zones {
	var (
		zs = newZones()
	)

	rand.Shuffle(len(fq), func(i, j int) {
		fq[i], fq[j] = fq[j], fq[i]
	})

	for _, v := range fq {
		zs.insert(v)
	}

	return zs
}
