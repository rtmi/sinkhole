#!/bin/sh

# steps to compile filter rules are collected here (so the configure hook can avoid breaking refresh/install)
SNCONF=$SNAP_DATA/sinkhole.toml
SNDENY=$SNAP_COMMON/deny.rules
SNALLOW=$SNAP_COMMON/allow.rules

cd $SNAP_DATA
# transform filters into rules for dnscrypt-proxy
$SNAP/bin/s6e -c=$SNCONF -rules=$SNAP_COMMON/rules
# rename rule output files
mv dnscrypt.bl $SNDENY
mv allow.dbd $SNALLOW

