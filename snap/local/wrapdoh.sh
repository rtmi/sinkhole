#!/bin/sh


export DOHIP="$(snapctl get doh-ip)"
export DOHPORT="$(snapctl get doh-port)"
export DOHUP="$(snapctl get doh-upstream)"
export DOHCF="$(snapctl get doh-knowncf)"

# todo config file instead of endless args line
exec $SNAP/bin/doh -ip=$DOHIP -port=$DOHPORT -upstream=$DOHUP -knowncf=$DOHCF
