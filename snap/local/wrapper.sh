#!/bin/sh

# todo? extract fields from the subdir $SNAP_DATA/etc/dnscache
export IP="$(snapctl get listen-ip)"
export UID="$(snapctl get user-id)"
export GID="$(snapctl get group-id)"
export IPSEND="$(snapctl get sender-ip)"
export CACHESIZE="$(snapctl get cache-size)"
export DATALIMIT="$(snapctl get data-limit)"

exec $SNAP/bin/dnscache
# man page indicates dnscache accepts seed of 128 bytes from stdin
##dd if=/dev/urandom bs=128 count=1 | $SNAP/bin/dnscache
