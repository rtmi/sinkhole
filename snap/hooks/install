#!/bin/sh -e

# TODO move to wrapper in order to utilize SNAP_USER_DATA
# TODO ss -lp 'sport = :domain'

SHCONF=$SNAP_DATA/sinkhole.toml
DCCONF=$SNAP_DATA/dnscrypt-proxy.toml
SHADDR=$(hostname -I | awk '{print $1}')
SHSUBNET=$(echo $SHADDR | awk -F. '{print $1"."$2"."$3}')

# Run-once default config
if [ -f $SHCONF ]; then
  exit;
fi

# default ootb caching with dnscrypt-proxy
snapctl stop --disable s6e.doh
snapctl stop --disable s6e.dnscache
snapctl set dns-cache=dnscrypt-proxy

# Start with example config
cp $SNAP/default-sinkhole.toml $SHCONF
cp $SNAP/default-dnscrypt-proxy.toml $DCCONF
cp -R $SNAP/etc/dnscache $SNAP_COMMON
cp $SNAP/*.txt $SNAP_DATA

# choose DoH upstream
sed -i -E "/^# server_names/a server_names=['cloudflare','cloudflare-ipv6']" $DCCONF
# net interface
sed -i -E "s/127\.0\.0\.1\:53/0\.0\.0\.0\:53/" $DCCONF

# make the subdir writable for file creation
chmod -f a+rw $SNAP_COMMON/dnscache

# extract IP address for dnscache to bind
snapctl set listen-ip=$SHADDR
# choose snap_daemon for UID
snapctl set user-id=584788
# choose snap_daemon for GID
snapctl set group-id=584788
# cache size
snapctl set cache-size=1000000
# data limit
snapctl set data-limit=3000000

# outgoing packets on high ports from this sender IP
# (in theory not used since we only forward to 127.0.0.1,
# but dnscache starts expecting the env var)
snapctl set sender-ip=$SHADDR

# network (most significant octets) to accept queries
snapctl set client-net=$SHSUBNET

# DoH daemon arguments
snapctl set doh-ip=127.0.0.1
snapctl set doh-port=53
snapctl set doh-upstream=1
snapctl set doh-knowncf=false
