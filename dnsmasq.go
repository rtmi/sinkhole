package sinkhole

import (
	"context"
	"golang.org/x/sync/errgroup"
)

const (
	msqtempl = `
address=/{{.}}/0.0.0.0`

	msqnxdomain = `
address=/{{.}}/`
)

// dnsmasqFormat performs the transformation in dnsmasq's address format
func dnsmasqFormat(ctx context.Context, g *errgroup.Group, hosts <-chan interface{}) {
	out1, out2 := tee(ctx.Done(), hosts)
	fill(ctx, g, out1, "dnsmasq.bl", msqtempl)
	fill(ctx, g, out2, "dnsmasq.nx", msqnxdomain)
}
