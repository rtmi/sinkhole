package sinkhole

import (
	"flag"
	toml "github.com/pelletier/go-toml"
	"os"
	"path/filepath"
	"regexp"
	"strings"
	"time"
)

const (
	SRC_NAME  = "name"
	SRC_URL   = "url"
	SRC_REGEX = "regexp"
	CFG_KEY   = "sinkhole"
	CFG_ADDS  = "local-additions"
	CFG_ALLOW = "allow"
	CFG_DBD   = "dbd"
	CFG_RULES = "rules"
	CFG_IRF   = "ignore-retrieval-failure"
	CFG_TMO   = "timeout"
	CFG_TRC   = "trace"
	CFG_OFF   = "offline"
	RE_127001 = `^127\.0\.0\.1\s+((?:[a-z0-9-]+\.)+[a-z]{3,63})(?:\s+\#.*)?$|^127\.0\.0\.1\s+((?:[a-z0-9-]+\.){2,}[a-z]{2})(?:\s+\#.*)?$|^127\.0\.0\.1\s+(\b\S{1,63}\.[a-z]{2})(?:\s+\#.*)?$`
	RE_ADBLK  = `^\|\|([a-z0-9-\.]+)\^`
	RE_GENRC  = `^([a-z0-9-\.]+)`
	CLI_ALLOW = "w"
	CLI_IRF   = "i"
	CLI_TMO   = "t"
	CLI_TMR   = "r"
	CLI_CFG   = "c"
	CLI_EXP   = "expiration"
	CLI_OFF   = "offline"
	CLI_TRC   = "trace"
	CLI_DBD   = "dbd"
	CLI_RULES = "rules"
)

var (
	re127001  = regexp.MustCompile(RE_127001)
	reAdblock = regexp.MustCompile(RE_ADBLK)
	reGeneric = regexp.MustCompile(RE_GENRC)
)

type Config struct {
	Dbl        *string
	Dwl        *string
	Dtr        *string
	Irf        *bool
	Uto        *time.Duration
	Expiration *time.Duration
	Offline    *string
	Trace      *bool
	DBD        *string
	RulesHome  *string
	next       int
	conf       *toml.Tree
	sources    []string
	wd         string
}

type source struct {
	offline string
	name    string
	url     string
	re      []string
	u       *regexp.Regexp
}

// instance the next filter source from the toml
func (c *Config) nextSource() (source, bool) {

	if c.next == len(c.sources) {
		return source{}, false
	}
	var k = c.sources[c.next]
	c.next++

	var t = c.conf.Get(k).(*toml.Tree)
	var s = source{}
	s.offline = c.offline()
	s.name = c.name(t)
	s.url = t.Get(SRC_URL).(string)
	s.re = c.re(t)
	s.u = c.patterns(s.re)
	return s, true
}

// Read the toml file and reset the next ptr
func (c *Config) initSources() (err error) {
	c.next = 0

	c.wd, err = os.Getwd()
	if err != nil {
		return
	}

	c.conf, err = toml.LoadFile(*c.Dbl)
	if err != nil {
		return
	}
	var cpath = c.offline()
	if cpath != "" {
		err = os.MkdirAll(cpath, 755)
	} else {
		*c.Offline = c.wd
	}

	// exclude configuration section
	var (
		n = 0
		a = c.conf.Keys()
	)
	for _, x := range a {
		if x != CFG_KEY {
			a[n] = x
			n++
		}
	}
	c.sources = a[:n]
	return
}

//  offline cached file
func (c Config) name(t *toml.Tree) string {
	return t.Get(SRC_NAME).(string)
}

// Path to local-additions file
func (c Config) localAdds() string {
	var t = c.conf.Get(CFG_KEY).(*toml.Tree)
	return t.Get(CFG_ADDS).(string)
}

// Path to allow filter file
func (c Config) allow() string {
	if c.hasArg(CLI_ALLOW) {
		return *c.Dwl
	}
	var t = c.conf.Get(CFG_KEY).(*toml.Tree)
	var setting = t.Get(CFG_ALLOW)
	if setting == nil {
		// toml line is commented out
		return *c.Dwl
	}
	var w = setting.(string)
	return w
}

// DBD mode creates the root/server subdir tree for dnscache
func (c Config) dbd() string {
	if c.hasArg(CLI_DBD) {
		return c.absPath(*c.DBD)
	}
	var t = c.conf.Get(CFG_KEY).(*toml.Tree)
	var setting = t.Get(CFG_DBD)
	if setting == nil {
		// toml line is commented out
		return ""
	}
	var w = setting.(string)
	return c.absPath(w)
}

func (c Config) rulesHome() string {
	if c.hasArg(CLI_RULES) {
		return c.absPath(*c.RulesHome)
	}
	var t = c.conf.Get(CFG_KEY).(*toml.Tree)
	var setting = t.Get(CFG_RULES)
	if setting == nil {
		// toml line is commented out
		return ""
	}
	var w = setting.(string)
	return c.absPath(w)
}

func (c Config) absPath(p string) string {
	if filepath.IsAbs(p) {
		return p
	}
	return filepath.Join(c.wd, p)
}

// Flag toggle to continue on URL error
func (c Config) skipping() bool {
	if c.hasArg(CLI_IRF) {
		return *c.Irf
	}
	var t = c.conf.Get(CFG_KEY).(*toml.Tree)
	var setting = t.Get(CFG_IRF)
	if setting == nil {
		// toml line is commented out
		return *c.Irf
	}
	var b = setting.(bool)
	return b
}

// timeout of http client
func (c Config) httpTimeout() time.Duration {
	if c.hasArg(CLI_TMO) {
		return *c.Uto
	}
	var t = c.conf.Get(CFG_KEY).(*toml.Tree)
	var setting = t.Get(CFG_TMO)
	if setting == nil {
		// toml line is commented out
		return *c.Uto
	}
	seconds, err := time.ParseDuration(setting.(string))
	if err != nil {
		panic("Error in TOML timeout setting " + err.Error())
	}
	return seconds
}

// Flag toggle to display tracing information
func (c Config) enableTracing() bool {
	if c.hasArg(CLI_TRC) {
		return *c.Trace
	}
	var t = c.conf.Get(CFG_KEY).(*toml.Tree)
	var setting = t.Get(CFG_TRC)
	if setting == nil {
		// toml line is commented out
		return *c.Trace
	}
	var b = setting.(bool)
	return b
}

// Path to cached files
func (c Config) offline() string {
	if c.hasArg(CLI_OFF) {
		return *c.Offline
	}
	var t = c.conf.Get(CFG_KEY).(*toml.Tree)
	var setting = t.Get(CFG_OFF)
	if setting == nil {
		// toml line is commented out
		return *c.Offline
	}
	var w = setting.(string)
	return w
}

func (c Config) hasArg(name string) bool {
	// Check whether CLI arg was actually specified, not just default
	// important because CLI arg takes precedence
	var found = false
	flag.Visit(func(f *flag.Flag) {
		if f.Name == name {
			found = true
		}
	})
	return found
}

// Cast from interface to string slice
func (c Config) re(t *toml.Tree) []string {
	var a = t.Get(SRC_REGEX).([]interface{})

	var r = make([]string, len(a))
	for k, v := range a {
		r[k] = v.(string)
	}
	return r
}

// Union of the reg expressions slice
func (c Config) patterns(re []string) *regexp.Regexp {
	switch {
	case len(re) == 1 && re[0] == "REGEXP127":
		return re127001
	case len(re) == 1 && re[0] == "REGEXPADBLK":
		return reAdblock
	case len(re) == 1 && re[0] == "REGEXPGENERIC":
		return reGeneric
	default:
		var u = strings.Join(re, "|")
		return regexp.MustCompile(u)
	}
}

// fqdn picks the sub/domain name from the line of text
func (s source) fqdn(txt string) string {
	// Use the reg expression specified in the TOML
	// for the data source.
	var (
		sm   = s.u.FindStringSubmatch(txt)
		sz   = len(sm)
		name = ""
	)
	switch {
	case sz > 1 && sm[1] != "":
		name = sm[1]
	case sz > 2 && sm[2] != "":
		name = sm[2]
	case sz > 3:
		name = sm[3]
	}

	return name
}
