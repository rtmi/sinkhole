package sinkhole

import (
	"bytes"
	"encoding/base64"
	"net/http"
	"net/url"
	"time"

	"github.com/miekg/dns"
	"github.com/pkg/errors"
)

type Upstream uint

const (
	None Upstream = iota
	GoogleDNS
	CloudflareDNS
	MozillaDNS
)

const (
	URI_TMPL_GOOGLE     = "https://dns.google/dns-query"
	URI_TMPL_CLOUDFLARE = "https://cloudflare-dns.com/dns-query"
	URI_TMPL_MOZILLA    = "https://mozilla.cloudflare-dns.com/dns-query"
	STUB_DNS_GOOGLE     = "8.8.8.8:53"
	STUB_DNS_CLOUDFLARE = "1.1.1.1:53"
)

type DoHClient struct {
	up         Upstream
	uritmpl    string
	dohURL     *url.URL
	method     string
	client     *http.Client
	cloudflare []string
}

func NewDoHClient(up Upstream, method string) (*DoHClient, error) {
	var (
		u string
		m string
	)

	// specified upstream
	switch up {
	case CloudflareDNS:
		u = URI_TMPL_CLOUDFLARE
	case MozillaDNS:
		u = URI_TMPL_MOZILLA
	case GoogleDNS:
		u = URI_TMPL_GOOGLE
	default:
		return &DoHClient{}, errors.New("DoH unknown upstream: " + string(up))
	}

	// specified method
	switch method {
	case http.MethodPost:
		m = http.MethodPost
	//case http.MethodGet:
	default:
		m = http.MethodGet
	}

	return &DoHClient{
		up:      up,
		uritmpl: u,
		method:  m,
		client: &http.Client{
			Timeout: 10 * time.Second,
		},
		cloudflare: []string{
			"1.1.1.1",
			"1.0.0.1",
			"162.159.132.53",
			"2606:4700:4700::1111",
			"2606:4700:4700::1001",
			"2606:4700:4700::64",
			"2606:4700:4700::6400",
			"162.159.36.1",
			"162.159.46.1",
		},
	}, nil
}

func (d *DoHClient) Query(r *dns.Msg) (*dns.Msg, error) {
	m := new(dns.Msg)

	// Extract question from inbound request
	q := r.Question[0]

	uri, err := d.encodeQuery(q.Name)
	if err != nil {
		return m, err
	}

	req, err := http.NewRequest(d.method, uri, nil)
	if err != nil {
		return m, err
	}

	req.Header.Set("Accept", "application/dns-message")
	resp, err := d.client.Do(req)
	if err != nil {
		return m, errors.Wrap(err, "DoH client send failed")
	}
	defer resp.Body.Close()

	switch resp.StatusCode {
	case 200:
		return d.unmarshal(m, resp)
	//case 301:	// moved
	//case 400:	// Bad Request
	//case 413:	// Payload Too Large
	//case 414:	// URI Too Long
	//case 415:	// Unsupported Media Type
	//case 429:	// Too Many Requests
	//case 500:	// Internal Server Error
	//case 501:	// Not Implemented
	//case 502:	// Bad Gateway
	default:
		return m, errors.New("DoH (non-200) response is not implemented: " + resp.Status)
	}
}

// initial stub lookup
func (d *DoHClient) PinDoH(knowncf bool) error {

	// extract fields from the DoH URI template
	dohurl, err := url.Parse(d.uritmpl)
	if err != nil {
		return errors.Wrap(err, "PinDoH failed to parse URL")
	}

	// send the DNS query to resolve DoH server's IP
	msg := new(dns.Msg)
	msg.SetQuestion(dns.Fqdn(dohurl.Host), dns.TypeA)
	cl := new(dns.Client)

	sdns, err := d.stubDNS()
	if err != nil {
		return errors.Wrap(err, "PinDoH stub DNS error")
	}

	in, _, err := cl.Exchange(msg, sdns)
	if err != nil {
		return errors.Wrap(err, "PinDoH failed lookup")
	}

	switch d.up {
	case CloudflareDNS, MozillaDNS:
		if knowncf {
			dohurl.Host = d.cloudflare[0]
			d.dohURL = dohurl

			return nil
		}
		// there may be many addresses in cloudflare result
		// cloudflare's x509 cert does not include their full list of IPs
		// we need to choose one that is known to be in their x509 cert
		for _, v := range in.Answer {
			if rec, ok := v.(*dns.A); ok {
				addr := rec.A.String()
				// check if address is in the known "good" list of cf IPs
				for _, cf := range d.cloudflare {
					if addr == cf {
						dohurl.Host = cf
						d.dohURL = dohurl

						return nil
					}
				}
			}
		}

	case GoogleDNS:
		if rec, ok := in.Answer[0].(*dns.A); ok {
			dohurl.Host = rec.A.String()
			d.dohURL = dohurl

			return nil
		}
	}
	return errors.New("PinDoH IP not found")
}

// read-only debugging info
func (d *DoHClient) RawTemplate() string {
	return d.uritmpl
}

// read-only debugging info
func (d *DoHClient) PinnedURI() string {
	return d.dohURL.String()
}

func (d *DoHClient) unmarshal(msg *dns.Msg, resp *http.Response) (*dns.Msg, error) {
	var buf bytes.Buffer
	if resp.ContentLength > 0 {
		num, err := buf.ReadFrom(resp.Body)

		if err != nil {
			return msg, errors.Wrap(err, "Unmarshal buffer error reading from response")
		}

		if num != resp.ContentLength {
			return msg, errors.New("Unmarshal buffer read incorrect length")
		}

		if err := msg.Unpack(buf.Bytes()); err != nil {
			return msg, errors.Wrap(err, "Unmarshal unpack failed")
		}
	}

	return msg, nil
}

func (d *DoHClient) encodeQuery(name string) (string, error) {
	// creating query
	// RFC8484 recommends ID 0 for DoH
	dns.Id = func() uint16 { return 0 }

	m := new(dns.Msg)
	m.RecursionDesired = true
	m.Question = make([]dns.Question, 1)
	m.Question[0] = dns.Question{dns.Fqdn(name), dns.TypeA, dns.ClassINET}

	// serialize to wire data
	data, err := m.Pack()
	if err != nil {
		return "", errors.Wrap(err, "DoH problem packing into wire data")
	}

	// use the base64url encoding without padding
	encoded := base64.RawURLEncoding.EncodeToString(data)

	if d.method == http.MethodGet {
		////u := d.uritmpl + "?dns=" + encoded
		u := d.dohURL.String() + "?dns=" + encoded
		return u, nil
	}

	return "", errors.New("DoH POST method not implemented")
}

func (d *DoHClient) stubDNS() (string, error) {
	switch d.up {
	case GoogleDNS:
		return STUB_DNS_GOOGLE, nil
	case CloudflareDNS:
		return STUB_DNS_CLOUDFLARE, nil
	case MozillaDNS:
		return STUB_DNS_CLOUDFLARE, nil
	}

	return "127.0.0.1", errors.New("DoH failed to match stub DNS to upstream: " + string(d.up))
}
