package sinkhole

import (
	"context"
	"golang.org/x/sync/errgroup"
)

const (
	unbtempl = `
local-zone: "{{.}}." redirect
local-data: "{{.}}. A 0.0.0.0"`

	unbnxdomain = `
local-zone: "{{.}}." always_nxdomain`
)

// unboundFormat performs the transformation in Unbound's zone format
func unboundFormat(ctx context.Context, g *errgroup.Group, hosts <-chan interface{}) {
	out1, out2 := tee(ctx.Done(), hosts)
	fill(ctx, g, out1, "unbound.bl", unbtempl)
	fill(ctx, g, out2, "unbound.nx", unbnxdomain)

}
