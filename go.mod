module github.com/patterns/sinkhole

go 1.14

require (
	github.com/miekg/dns v1.1.31
	github.com/pelletier/go-toml v1.8.0
	github.com/pkg/errors v0.9.1
	golang.org/x/crypto v0.0.0-20200728195943-123391ffb6de // indirect
	golang.org/x/net v0.0.0-20200707034311-ab3426394381 // indirect
	golang.org/x/sync v0.0.0-20200625203802-6e8e738ad208
	golang.org/x/sys v0.0.0-20200812155832-6a926be9bd1d // indirect
)
