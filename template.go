package sinkhole

import (
	"bufio"
	"context"
	"golang.org/x/sync/errgroup"
	"os"
	"text/template"
)

// fill interpolates host names into the template
func fill(ctx context.Context, g *errgroup.Group, hosts <-chan interface{}, filename, templ string) {

	g.Go(func() error {
		f, err := os.Create(filename)
		if err != nil {
			return err
		}
		defer f.Close()
		var t = template.Must(template.New(filename).Parse(templ))

		w := bufio.NewWriter(f)

		// rcv from channel
		for host := range hosts {
			// apply template with each host name
			var err = t.Execute(w, host.(string))
			if err != nil {
				return err
			}

			select {
			case <-ctx.Done():
				w.Flush()
				return ctx.Err()
			default:
			}
		}
		w.Flush()
		return nil
	})

}
