package sinkhole

import (
	"context"
	"crypto/tls"
	"github.com/pkg/errors"
	"io"
	"log"
	"net/http"
	"net/http/httptrace"
	"os"
	"path/filepath"
	"strconv"
	"time"
)

const (
	_                   = iota
	uncached fetchState = 1
	fetching
	promoting
	cached
)

type fetchState int

// transport is an http.RoundTripper that implements hooks to report
// HTTP tracing events
type transport struct {
	conf    *Config
	current *http.Request
	timeout time.Duration
	state   fetchState
	err     error
}

func newTransport(c *Config) *transport {
	return &transport{
		conf:    c,
		timeout: c.httpTimeout(),
		state:   uncached,
		err:     nil,
	}
}

// Access the filter rules and return a reader
func (t *transport) fetch(ctx context.Context, src source) (io.Reader, error) {
	var (
		err  error
		rdr  io.Reader
		name = t.formatName(src)
	)
	// first pass, dial URL
	if t.state == uncached {
		if err = t.fetchURL(ctx, src); err != nil {
			t.err = errors.Wrap(err, "Fetch URL failed")
		} else {
			if err = t.promoteToCache(src); err != nil {
				t.err = errors.Wrap(err, "Fetch promote cache failed")
			} else {
				t.state = cached
			}
		}
	}

	// URL and cache success
	if t.state == cached {
		return os.Open(name)
	}

	if err = t.recoverCache(src); err != nil {
		if t.err != nil {
			return rdr, errors.Wrap(t.err, "Recover failed")
		}
		return rdr, err
	}

	// continue with fallback
	if rdr, err = os.Open(name); err == nil {
		return rdr, nil
	} else {
		if t.err != nil {
			return rdr, errors.Wrap(t.err, "Fetch cache file inconsistent")
		}
		return rdr, errors.Wrap(err, "Fetch fallback file error")
	}
}

func (t *transport) fetchURL(ctx context.Context, src source) error {
	// Retrieve filter rules from URL
	var cx = ctx

	if t.conf.enableTracing() {
		trace := &httptrace.ClientTrace{
			GotConn:              t.GotConn,
			DNSDone:              t.DNSDone,
			DNSStart:             t.DNSStart,
			TLSHandshakeStart:    t.TLSHandshakeStart,
			TLSHandshakeDone:     t.TLSHandshakeDone,
			GotFirstResponseByte: t.GotFirstResponseByte,
		}
		cx = httptrace.WithClientTrace(ctx, trace)
	}

	req, err := http.NewRequestWithContext(cx, "GET", src.url, nil)
	if err != nil {
		return errors.Wrap(err, "Fetch failed request: "+src.url)
	}

	client := &http.Client{Transport: t, Timeout: t.timeout}
	resp, err := client.Do(req)
	if err != nil {
		return errors.Wrap(err, "Fetch failed on default client: "+src.url)
	}
	defer resp.Body.Close()

	log.Printf("Fetch: %s", src.url)
	t.state = fetching
	tmp := t.formatTmp(src)
	t.debug("Creating temp file %s", tmp)
	out, err := os.Create(tmp)
	if err != nil {
		return errors.Wrap(err, "Fetch tmp failed: "+tmp)
	}
	defer out.Close()
	t.debug("Saving response to %s", tmp)
	if _, err = io.Copy(out, resp.Body); err != nil {
		return errors.Wrap(err, "Fetch copy failed: "+tmp)
	}
	return nil
}

func (t *transport) promoteToCache(src source) error {
	//rename tmp file to overwrite old file
	nam := t.formatName(src)
	tmp := t.formatTmp(src)
	bak := t.formatBak(src)
	err := os.Rename(nam, bak)
	if os.IsNotExist(err) || err == nil {
		t.state = promoting
		t.debug("Moved cache file %s aside to bak", nam)
		err = os.Rename(tmp, nam)
		if err != nil {
			return errors.Wrap(err, "Fetch rename failed to overwrite: "+tmp)
		}
		t.debug("Swapped in %s as the new file", tmp)
		os.Remove(bak)
		t.debug("Remove old %s as no longer needed", bak)
	} else {
		//renaming old file failed
		return errors.Wrap(err, "Fetch rename failed to backup: "+bak)
	}
	return nil
}

func (t *transport) recoverCache(src source) error {
	// For when the promote-to-cache is unsuccessful
	// 1. *.bak file needs to be returned to the correct file name
	// 2. tmp file can't overwrite, extraneous file (low priority)
	// 3. old file isn't deleted (low priority)
	var (
		err error
		nam = t.formatName(src)
		tmp = t.formatTmp(src)
		bak = t.formatBak(src)
	)
	if err = os.Rename(bak, nam); os.IsNotExist(err) {
		// Backup not found (possible error occured before fetching)
		if _, err2 := os.Stat(nam); os.IsNotExist(err2) {
			// also missing (this is bad)
			err = errors.Wrap(err, "Recover can't find backup or cache file")
		} else {
			err = nil
		}
	} else {
		// Backup file exists, maybe destination file is preventing rename
		// so remove it first
		os.Remove(nam)
		if err = os.Rename(bak, nam); err != nil {
			err = errors.Wrap(err, "Recover can't return backup")
		}
	}
	os.Remove(tmp)

	return err
}

func (t *transport) formatName(src source) string {
	return filepath.Join(
		src.offline,
		src.name,
	)
}
func (t *transport) formatTmp(src source) string {
	return filepath.Join(
		src.offline,
		strconv.Itoa(int(t.state))+"-"+src.name,
	)
}
func (t *transport) formatBak(src source) string {
	return filepath.Join(
		src.offline,
		strconv.Itoa(int(t.state))+"-"+src.name+".bak",
	)
}

func (t *transport) debug(fmt string, p interface{}) {
	if t.conf.enableTracing() {
		log.Printf(fmt, p)
	}
}

// RoundTrip wraps http.DefaultTransport.RoundTrip
func (t *transport) RoundTrip(req *http.Request) (*http.Response, error) {
	t.current = req
	return http.DefaultTransport.RoundTrip(req)
}

// GotConn prints connection info (Reused indicates followed redirects)
func (t *transport) GotConn(info httptrace.GotConnInfo) {
	log.Printf("Connected (%v): %+v", t.current.URL, info)
}

// DNSDone prints DNS info
func (t *transport) DNSDone(dnsInfo httptrace.DNSDoneInfo) {
	log.Printf("DNS Done: %+v", dnsInfo)
}

func (t *transport) DNSStart(dnsInfo httptrace.DNSStartInfo) {
	log.Printf("DNS Start: %+v", dnsInfo)
}

func (t *transport) TLSHandshakeStart() {
	log.Println("TLS Start")
}

func (t *transport) TLSHandshakeDone(stat tls.ConnectionState, e error) {
	log.Printf("TLS Done: %v ; %v", stat, e)
}

func (t *transport) GotFirstResponseByte() {
	log.Println("Response Start")
}
