ARG ARCH=
ARG RELEASE=2.0.44
ARG PORT=5300
ARG ZONES=/opt/dns/deny.rules
ARG ALLOW=/opt/dns/allow.rules
ARG CONF=/opt/dns/dnscrypt-proxy.toml

FROM ${ARCH}golang:1.15-alpine3.12 AS build-env
ARG RELEASE
ARG PORT
ARG ZONES
ARG ALLOW
ARG CONF
ENV RELEASE=${RELEASE}
ENV PORT=${PORT}
ENV ZONES=${ZONES}
ENV ALLOW=${ALLOW}
ENV CONF=${CONF}
RUN apk update && apk --no-cache add autoconf automake libtool git \
        build-base gcc abuild binutils cmake linux-headers sed; 
RUN git clone --depth 1 --single-branch --branch ${RELEASE} \
    https://github.com/DNSCrypt/dnscrypt-proxy /opt/src
WORKDIR /opt/src/dnscrypt-proxy
RUN go build -ldflags="-s -w" -mod vendor ;\
    mv example-dnscrypt-proxy.toml ex.toml ;\
    sed -i -E "/^# server_names/a server_names=['cloudflare','cloudflare-ipv6']" ex.toml ;\
    sed -i -E "s@# blacklist_file = 'blacklist\.txt@ blacklist_file='${ZONES}@" ex.toml ;\
    sed -i -E "s@# whitelist_file = 'whitelist\.txt@ whitelist_file='${ALLOW}@" ex.toml ;\
    sed -i -E "s/127\.0\.0\.1\:53/0\.0\.0\.0\:${PORT}/" ex.toml ;\
    mv ex.toml /opt/
COPY . /opt/s6e
WORKDIR /opt/s6e
RUN go build -o s6e cmd/s6e/*.go ;\
    cp /opt/src/utils/generate-domains-blacklists/*.txt . ;\
    ./s6e -c=snap/local/sinkhole.toml -rules=snap/local/rules ;\
    mv dnscrypt.bl /opt/deny.dbd ;\
    cp allow.dbd /opt/


# final stage
FROM ${ARCH}alpine:3.12
ARG PORT
ARG ZONES
ARG ALLOW
ARG CONF
ENV PORT=${PORT}
ENV ZONES=${ZONES}
ENV ALLOW=${ALLOW}
ENV CONF=${CONF}
COPY --from=build-env /opt/src/dnscrypt-proxy/dnscrypt-proxy /opt/dns/
COPY --from=build-env /opt/deny.dbd ${ZONES}
COPY --from=build-env /opt/allow.dbd ${ALLOW}
COPY --from=build-env /opt/ex.toml ${CONF}
EXPOSE ${PORT}
ENTRYPOINT ["/opt/dns/dnscrypt-proxy"]

