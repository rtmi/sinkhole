// typically bind to loopback 127.0.0.1:53
// accept DNS queries forwarded by dnscache
// send the queries via DoH to Google DNS
// use env to watch behavior GODEBUG=http2debug=1

package main

import (
	"context"
	"flag"
	"fmt"
	"net/http"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/miekg/dns"
	"github.com/patterns/sinkhole/doh"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

func main() {
	var (
		ip      = flag.String("ip", "127.0.0.1", "IP address to bind, usually the first loopback interface")
		port    = flag.Int("port", 53, "port to listen on")
		up      = flag.Int("upstream", 1, "1: GoogleDNS, 2: CloudflareDNS, 3: MozillaDNS")
		method  = flag.String("method", http.MethodGet, "only GET is implemented")
		knowncf = flag.Bool("knowncf", false, "Do not lookup Cloudflare upstream and pick from known list. Only applies for upstream 2/3 options")
		srvudp  *dns.Server
		srvtcp  *dns.Server
	)
	flag.Parse()
	g, gcx := errgroup.WithContext(context.Background())

	doh, err := sinkhole.NewDoHClient(itoUpstream(*up), *method)
	if err != nil {
		fmt.Printf("DoH new instance failed - %v\n", err)
		return
	}

	fmt.Printf("Stub resolver lookup - %v\n", doh.RawTemplate())
	if err := doh.PinDoH(*knowncf); err != nil {
		fmt.Printf("DoH stub lookup failed - %v\n", err)
		return
	}
	fmt.Printf("Stub resolver result - %v\n", doh.PinnedURI())

	// wildcard handler forwards to the DoH upstream
	dns.HandleFunc(".", func(w dns.ResponseWriter, r *dns.Msg) {
		// remember the ID from the request because
		// in DoH we zero the field
		id := r.MsgHdr.Id

		m, err := doh.Query(r)

		if err != nil {
			fmt.Printf("DoH upstream error - %v\n", err)
			return
		}

		// restore the ID
		m.MsgHdr.Id = id
		w.WriteMsg(m)
	})

	defer dns.HandleRemove(".")

	g.Go(func() error {
		fmt.Printf("Start doh udp listening on %s:%d\n", *ip, *port)
		srvudp = &dns.Server{Addr: *ip + ":" + strconv.Itoa(*port), Net: "udp"}
		if err := srvudp.ListenAndServe(); err != nil {
			return errors.Wrap(err, "Failed to set udp listener")
		}
		return nil
	})

	g.Go(func() error {
		fmt.Printf("Start doh tcp listening on %s:%d\n", *ip, *port)
		srvtcp = &dns.Server{Addr: *ip + ":" + strconv.Itoa(*port), Net: "tcp"}
		if err := srvtcp.ListenAndServe(); err != nil {
			return errors.Wrap(err, "Failed to set tcp listener")
		}
		return nil
	})

	// start a goroutine to watch ctl-C
	g.Go(func() error {
		var sig = make(chan os.Signal)
		defer close(sig)
		signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
		<-sig
		// teardown udp/tcp listeners
		ctx, _ := context.WithTimeout(gcx, 5*time.Second)
		srvudp.ShutdownContext(ctx)
		srvtcp.Shutdown()
		return nil
	})

	// block until goroutines finish
	if err := g.Wait(); err != nil {
		fmt.Printf("Group error - %v\n", err)
	}
	fmt.Printf("Shutdown done\n")
}

func itoUpstream(up int) sinkhole.Upstream {
	switch up {
	case 1:
		return sinkhole.GoogleDNS
	case 2:
		return sinkhole.CloudflareDNS
	case 3:
		return sinkhole.MozillaDNS
	}

	fmt.Printf("Unknown upstream option. Only 1 (GoogleDNS), 2 (ClouflareDNS), 3 (MozillaDNS) are valid")
	return sinkhole.None
}

func setUID(uid int) {
	_, _, serr := syscall.Syscall(syscall.SYS_SETUID, uintptr(uid), 0, 0)
	if serr != 0 {
		panic(serr)
		//os.Exit(1)
	}

	fmt.Printf("Real UID: %d\n", syscall.Getuid())
	fmt.Printf("Effective UID: %d\n", syscall.Geteuid())
}
