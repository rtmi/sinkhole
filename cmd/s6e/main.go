package main

import (
	"context"
	"flag"
	"log"
	"time"

	"github.com/patterns/sinkhole"
)

func main() {
	var (
		err    error
		ctx    context.Context
		cancel context.CancelFunc
		conf   = readArgs()
	)
	ctx, cancel = context.WithTimeout(context.Background(), *conf.Expiration)
	defer cancel()

	err = sinkhole.Run(ctx, conf)
	if err != nil {
		log.Fatal(err)
	}

	log.Printf("Everything nominal")
}

func readArgs() *sinkhole.Config {
	var conf = &sinkhole.Config{
		Dbl:        flag.String(sinkhole.CLI_CFG, "sinkhole.toml", "definition of sources for deny filters"),
		Dwl:        flag.String(sinkhole.CLI_ALLOW, "allow.txt", "allow filters to apply on input"),
		Dtr:        flag.String(sinkhole.CLI_TMR, "domains-time-restricted.txt", "file containing a set of names to be time restricted"),
		Irf:        flag.Bool(sinkhole.CLI_IRF, true, "generate list even if some urls couldn't be retrieved"),
		Uto:        flag.Duration(sinkhole.CLI_TMO, 30*time.Second, "URL open timeout"),
		Expiration: flag.Duration(sinkhole.CLI_EXP, 20*time.Second, "Program timeout expiration to prevent runaway process"),
		Offline:    flag.String(sinkhole.CLI_OFF, "", "Path to saved cache of remote sources"),
		Trace:      flag.Bool(sinkhole.CLI_TRC, false, "Enable tracing information"),
		DBD:        flag.String(sinkhole.CLI_DBD, "", "Path to the dnscache root/servers subdir"),
		RulesHome:  flag.String(sinkhole.CLI_RULES, "rules", "Path to the rules directory"),
	}

	flag.Parse()
	return conf
}
