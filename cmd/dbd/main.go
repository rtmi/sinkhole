// To accept requests from dnscache, and forward to cloudflared.
// But before passing to cloudflared, apply deny/allow rules.

package main

import (
	"context"
	"flag"
	"log"
	"net"
	"os"
	"os/signal"
	"strconv"
	"syscall"
	"time"

	"github.com/miekg/dns"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
)

const (
	SOA      = "@ SOA aluminum jazz.artemis.space. 2018110201 3600 1800 604800 86400"
	DBD_NOTE = "Default deny applies in absence of an explicit allow rule"
)

func main() {
	var (
		ip     = flag.String("ip", "127.0.0.1", "IP address to bind, usually the second loopback interface")
		port   = flag.Int("port", 8053, "port to listen on")
		srvudp *dns.Server
		srvtcp *dns.Server
	)
	flag.Parse()
	g, gcx := errgroup.WithContext(context.Background())

	dns.HandleFunc(".", denyByDefault)
	defer dns.HandleRemove(".")

	g.Go(func() error {
		log.Printf("Start dbd udp listening on %s:%d", *ip, *port)
		srvudp = &dns.Server{Addr: *ip + ":" + strconv.Itoa(*port), Net: "udp"}
		if err := srvudp.ListenAndServe(); err != nil {
			return errors.Wrap(err, "Failed to set udp listener")
		}
		return nil
	})

	g.Go(func() error {
		log.Printf("Start dbd tcp listening on %s:%d", *ip, *port)
		srvtcp = &dns.Server{Addr: *ip + ":" + strconv.Itoa(*port), Net: "tcp"}
		if err := srvtcp.ListenAndServe(); err != nil {
			return errors.Wrap(err, "Failed to set tcp listener")
		}
		return nil
	})

	// start a goroutine to watch ctl-C
	g.Go(func() error {
		var sig = make(chan os.Signal)
		defer close(sig)
		signal.Notify(sig, syscall.SIGINT, syscall.SIGTERM)
		<-sig
		// teardown udp/tcp listeners
		ctx, _ := context.WithTimeout(gcx, 5*time.Second)
		srvudp.ShutdownContext(ctx)
		srvtcp.Shutdown()
		return nil
	})

	// block until goroutines finish
	if err := g.Wait(); err != nil {
		log.Printf("Group error - %v", err)
	}
	log.Printf("Shutdown done")
}

// Deny-by-default DNS answer message
func denyByDefault(w dns.ResponseWriter, r *dns.Msg) {
	// make authority section
	auth, _ := dns.NewRR("$TTL 86400\n" + SOA)

	// Extract question from inbound request
	q := r.Question[0]

	// make answer section (redirect to 0.0.0.0)
	answer := new(dns.A)
	answer.Hdr = dns.RR_Header{Name: q.Name, Rrtype: dns.TypeA, Class: dns.ClassINET, Ttl: 3600}
	answer.A = net.IPv4(0, 0, 0, 0)

	// make txt section
	note := &dns.TXT{
		Hdr: dns.RR_Header{Name: q.Name, Rrtype: dns.TypeTXT, Class: dns.ClassINET, Ttl: 3600},
		Txt: []string{DBD_NOTE},
	}

	m := new(dns.Msg)
	m.SetReply(r)
	m.Authoritative = true
	m.Ns = []dns.RR{auth}
	m.Answer = []dns.RR{answer}
	m.Extra = []dns.RR{note}
	w.WriteMsg(m)
}
