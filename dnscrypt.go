package sinkhole

import (
	"bufio"
	"context"
	"fmt"
	"github.com/pkg/errors"
	"golang.org/x/sync/errgroup"
	"os"
	"path/filepath"
	"time"
)

const (
	DBD_ALLOW       = "allow"
	DBD_ALLOWTMP    = "allow.tmp"
	DBD_ALLOWOUT    = "allow.dbd"
	FMT_DNSCRYPTOUT = "dnscrypt.bl"
	FMT_DNSCRYPT    = `
{{.}}`
)

// dnscryptFormat performs the transformation in dnscrypt's filter format
func dnscryptFormat(ctx context.Context, g *errgroup.Group, hosts <-chan interface{}) {
	fill(ctx, g, hosts, FMT_DNSCRYPTOUT, FMT_DNSCRYPT)
}

func dbd(ctx context.Context, cfg *Config) error {
	if err := dnscacheServers(ctx, cfg); err != nil {
		return err
	}
	return dnscryptHosts(ctx, cfg)
}

func dnscryptHosts(ctx context.Context, cfg *Config) error {
	// Think of firewalls.
	// Choosing the DBD option means:
	// 1. one deny rule (block *)
	// 2. only enable trusted zones (selective allow)

	// output for deny rule
	bl, err := os.Create(FMT_DNSCRYPTOUT)
	if err != nil {
		return errors.Wrap(err, "DBD create file failed: "+FMT_DNSCRYPTOUT)
	}
	defer bl.Close()
	_, err = bl.WriteString("*.*")
	if err != nil {
		return errors.Wrap(err, "DBD file write failed: "+FMT_DNSCRYPTOUT)
	}

	// open temp file for allow rules
	tmp, err := os.Create(DBD_ALLOWTMP)
	if err != nil {
		return errors.Wrap(err, "DBD create tmp failed: "+DBD_ALLOWTMP)
	}
	defer tmp.Close()

	err = eachAllowRule(cfg.rulesHome(), func(r string) error {
		_, werr := tmp.WriteString(fmt.Sprintln(r))
		if werr != nil {
			return errors.Wrap(werr, "DBD error writing: "+DBD_ALLOWTMP)
		}

		return nil
	})
	tmp.Sync()
	// temp file completed, replace old one
	var now = time.Now()
	var bak = fmt.Sprintf("allow.%02d%02d", now.Month(), now.Day())
	if err = os.Rename(DBD_ALLOWOUT, bak); err == nil || os.IsNotExist(err) {
		err = os.Rename(DBD_ALLOWTMP, DBD_ALLOWOUT)
		if err != nil {
			return errors.Wrap(err, "DBD replace error: "+DBD_ALLOWOUT)
		}
		return nil
	}

	return errors.Wrap(err, "DBD error making backup: "+DBD_ALLOWOUT)

}

func dnscacheServers(ctx context.Context, cfg *Config) error {
	// create the files at root/servers where the @ file is found
	// each file is named according to its zone and contains the
	// list of (cache) server IPs that can answer DNS for the zone

	cpath := cfg.dbd()
	if cpath == "" {
		return nil
	}

	err := eachAllowRule(cfg.rulesHome(), func(r string) error {
		// create file named for the rule
		// with content of 127.0.0.1

		f, err := os.Create(filepath.Join(cpath, r))
		if err != nil {
			return err
		}
		defer f.Close()
		_, err = f.WriteString("127.0.0.1")
		if err != nil {
			return err
		}
		f.Sync()
		return nil
	})
	return err
}

func eachAllowRule(home string, rulestep func(r string) error) error {
	// track zones with set
	var zo = newZones()
	var subdir = filepath.Join(home, DBD_ALLOW)
	var rule string

	err := filepath.Walk(subdir, func(path string, info os.FileInfo, err error) error {
		switch {
		case err != nil:
			return err
		case info.Size() > 0 && !info.IsDir():
			// visited file
			r, rerr := os.Open(path)
			if rerr != nil {
				return errors.Wrap(rerr, "DBD error reading: "+path)
			}
			defer r.Close()

			scanner := bufio.NewScanner(r)
			for scanner.Scan() {
				rule = reGeneric.FindString(scanner.Text())
				if rule == "" {
					continue
				}
				if zo.lookup(rule) {
					// already have zone, skip
					continue
				}
				zo.insert(rule)

				if derr := rulestep(rule); derr != nil {
					return errors.Wrap(derr, "DBD error processing: "+rule)
				}
			}

			if serr := scanner.Err(); serr != nil {
				return errors.Wrap(serr, "DBD scanner error")
			}
			return nil
		}
		return nil
	})
	if err != nil {
		return errors.Wrap(err, "DBD error walking the subdir: "+subdir)
	}
	return nil
}
