package sinkhole

import (
	"fmt"
	"regexp"
	"strings"
	"sync"
)

type zones struct {
	sync.RWMutex
	internal map[string]bool
}

const (
	DNS_TLD  = `\.?([a-z0-9-]+)$`
	DNS_ZONE = `([a-z0-9-]+\.[a-z0-9]{3,63})$|([a-z0-9-]+\.[a-z]{2,3}\.[a-z]{2})$|([a-z0-9-]+\.[a-z]{2})$`
)

var (
	reTLD  = regexp.MustCompile(DNS_TLD)
	reZone = regexp.MustCompile(DNS_ZONE)
)

func newZones() zones {
	return zones{
		internal: make(map[string]bool),
	}
}

func (z zones) lookup(fqdn string) bool {
	// The fqdn is already a member when:
	// a) its ancestor is a member
	// b) or it is a member

	// Zone name is the highest ancestor
	var zo = z.zone(fqdn)
	if zo != "" {
		if _, ok := z.r(zo); ok {
			// our favorite, the level-1 / zone is already a set member
			return true
		}
	}

	// Parent domain is the nearest ancestor
	var pd = z.super(fqdn)
	if pd != "" && pd != zo {
		if _, ok := z.r(pd); ok {
			return true
		}
	}

	// by itself
	if _, ok := z.r(fqdn); ok {
		return true
	}

	return false
}

func (z zones) insert(fqdn string) {
	z.w(fqdn)
}

// TODO change into a ForEach that runs a specified callback
func (z zones) members() []string {
	var m = []string{}
	z.RLock()
	for zo, _ := range z.internal {
		m = append(m, zo)
	}
	z.RUnlock()
	return m
}

func (z zones) deduplicate() {
	// make final clean-up sweep
	// range through the zones, and prune descendants (and subtrees of super-domains)
	var zo string
	for k, _ := range z.internal {
		zo = z.zone(k)
		if zo == k {
			continue
		}
		if z.lookup(zo) || z.lookup(z.super(k)) {
			z.Lock()
			delete(z.internal, k)
			z.Unlock()
		}
	}
}

func (z zones) tld(fqdn string) string {
	var s = reTLD.FindStringSubmatch(fqdn)
	if s == nil || len(s) < 2 {
		return ""
	}
	return s[1]
}

// extract the zone name (1 level below TLD)
func (z zones) zone(fqdn string) string {
	var (
		sm = reZone.FindStringSubmatch(fqdn)
		sz = len(sm)
		pd = ""
	)
	switch {
	case sz > 1 && sm[1] != "":
		pd = sm[1]
	case sz > 2 && sm[2] != "":
		pd = sm[2]
	case sz > 3:
		pd = sm[3]
	}

	return pd
}

func (z zones) super(fqdn string) string {
	// Extract the parent of a subdomain
	// It's the result of chopping the name left of the leftmost dot.
	// The result can also equal the zone name (level-1).

	var i = strings.IndexRune(fqdn, '.')
	if i != -1 && i+1 < len(fqdn) {
		return fqdn[i+1:]
	}

	return ""
}

// TODO better terminology?
func (z zones) truncate(fqdn string) string {
	// drop the level-1 / zone name from the subdomain
	var zo = z.zone(fqdn)
	if zo == "" {
		return ""
	}
	var host = strings.TrimSuffix(fqdn, zo)
	return host
}

// r makes a read-only access on the internal map
func (z zones) r(fqdn string) (bool, bool) {
	z.RLock()
	var val, ok = z.internal[fqdn]
	z.RUnlock()
	return val, ok
}

// w is the mutate call on the internal map
func (z zones) w(fqdn string) {
	z.Lock()
	z.internal[fqdn] = true
	z.Unlock()
}

func (z zones) String() string {
	// DEBUG expose info about the state of the set
	var b strings.Builder

	for k, v := range z.internal {
		fmt.Fprintf(&b, "\n%s: %v", k, v)
	}

	return fmt.Sprintf("zones members size: %d \n %v \n", len(z.internal), b.String())
}
