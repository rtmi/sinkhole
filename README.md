[![s6e](https://snapcraft.io//s6e/badge.svg)](https://snapcraft.io/s6e)
# sinkhole

Following the generate-domains-blacklist.py Python script 
 that is part of
 [dnscrypt-proxy](https://github.com/dnscrypt/dnscrypt-proxy).
 Started as steps from github.com/oznu/dns-zone-blacklist
 using Golang.


## Quickstart
1. Install [Docker](https://docker.com/)
2. Git clone the [repo](https://github.com/patterns/sinkhole/)
3. Build and run:
```bash
$ cd sinkhole
$ docker build -t sinkhole .
$ docker run -ti -p 5300:5300/udp sinkhole
```

4. Baseline DNS:
```bash
$ dig @127.0.0.1 -p 5300 adafruit.com
```

Which shows the domain has round-robin between `104.20.38.240` and `104.20.39.240`. So far so good.

5. Verify blocking:
```bash
$ dig @127.0.0.1 -p 5300 doubleclick.net
```

The result will be 0.0.0.0 which means success!

### Port
To choose the port, either pass a build arg to docker or use your own config file.

Example of the build arg:
```bash
$ docker build --build-arg PORT=5353 -t sinkhole .
```

Example of the config file approach:
```bash
$ curl -o dns.toml https://github.com/DNSCrypt/dnscrypt-proxy/blob/master/dnscrypt-proxy/example-dnscrypt-proxy.toml
$ nano dns.toml
#search for the bind line
    [127.0.0.1:53]
$ docker run -ti -p 5353:5353/udp -v $(pwd):/opt/mydns sinkhole -c /opt/mydns/dns.toml
```

[![Get it from the Snap Store](https://snapcraft.io/static/images/badges/en/snap-store-white.svg)](https://snapcraft.io/s6e)
## Credits

dnscache from
  [djbdns](https://cr.yp.to/djbdns/blurb/cache.html)

cloudflared Argo Tunnel
  by [CloudFlare](https://github.com/cloudflare/cloudflared)([LICENSE](https://github.com/cloudflare/cloudflared/blob/master/LICENSE))

DNS library
  by [Miek Gieben](https://github.com/miekg/dns)([LICENSE](https://github.com/miekg/dns/blob/master/LICENSE))

dnscrypt-proxy
  by [Frank Denis](https://github.com/dnscrypt/dnscrypt-proxy)([LICENSE](https://github.com/DNSCrypt/dnscrypt-proxy/blob/master/LICENSE))

Unbound image
 by [Kyle Harding](https://gitlab.com/klutchell/unbound)([LICENSE](https://gitlab.com/klutchell/unbound/blob/master/LICENSE))

[BIND, Dnsmasq, Unbound blacklist project](https://github.com/oznu/dns-zone-blacklist)

Unified hosts file
 by [Steven Black](https://github.com/StevenBlack/hosts) ([LICENSE](https://github.com/StevenBlack/hosts/blob/master/license.txt))

[Pi-hole guide on recursive DNS](https://docs.pi-hole.net/guides/unbound/)

[Archlinux config of Unbound](https://wiki.archlinux.org/index.php/unbound#Domain_blacklisting)

[sync/errgroup](https://www.oreilly.com/learning/run-strikingly-fast-parallel-file-searches-in-go-with-sync-errgroup)


## Notes

```
- raspberry pi 4
- ubuntu-server arm64 image
- ip a (wlan0)
- /etc/netplan/wireless.yaml (from /usr/share/doc/cloud-init/examples)
- sudo netplan --debug try/generate/apply
- sudo apt update -y && sudo apt upgrade -y && sudo apt dist-upgrade -y
- sudo apt install -y unbound
- sudo vi /etc/unbound/unbound.conf.d/pihole.conf (see pi-hole guide for unbound recursive dns)
- curl -o unbound.bl https://raw.githubusercontent.com/oznu/dns-zone-blacklist/master/unbound/unbound.blacklist
- sudo systemctl stop systemd-resolved
- sudo vi /etc/systemd/resolved.conf
- sudo ln -sf /run/systemd/resolve/resolv.conf /etc/resolv.conf
- sudo ufw allow in on wlan0 from 192.168.0.0/16 to any port 53
- sudo ufw enable
- sudo ufw status
- sudo echo "127.0.0.1 pihole" >> /etc/hosts
- sudo rm /etc/hostname && echo pihole > /etc/hostname
- optionally, touch /etc/cloud/cloud-init.disabled
```

